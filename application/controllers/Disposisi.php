<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Disposisi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("disposisi_m");
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $disposisi = $this->disposisi_m;
        $validation = $this->form_validation;
        $validation->set_rules($disposisi->rules());
        if ($validation->run()) {
            $disposisi->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Surat Disposisi berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("disposisi");
        }
        $data["title"] = "Disposisi";
        $data["surat_masuk"] = $this->disposisi_m->getSuratMasuk();
        $data["data_disposisi"] = $this->disposisi_m->getAll();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('disposisi/index', $data);
        $this->load->view('templates/footer');
            
    }

    public function edit($id = null)
    {
        $this->form_validation->set_rules('id_surat_masuk', 'id_surat_masuk', 'required');
        $this->form_validation->set_rules('tgl_surat', 'tgl_surat', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Surat Disposisi gagal diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('disposisi');
        }else {
            $update = array(
                "id_surat_masuk" => $this->input->post('id_surat_masuk'),
                "tgl_surat" => $this->input->post('tgl_surat'),
                "tgl_update" => date('Y-m-d H:i:s'),
                "user_update_by" => $this->session->userdata['username']
            );
            $this->db->where('id', $id);
            $this->db->update('surat_disposisi', $update);
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Surat Disposisi berhasil diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('disposisi');
        }

    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Surat Disposisi gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('disposisi');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('surat_disposisi');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Surat Disposisi berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('disposisi');
        }
    }

}
