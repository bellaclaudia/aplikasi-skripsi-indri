<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ganti_pass extends CI_Controller
{

    public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('ganti_pass_m');
		if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
	}       
	
	public function index()
	{
		$data['master_user'] = $this->db->get_where('master_user', ['username' => $this->session->userdata('username')])->row_array();
		// $userpass = $data['master_user']['userpass'];
		// print_r($userpass); die();
		$this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('ganti_password', $data);
        $this->load->view('templates/footer');
	}

	public function changepassword(){

		$data['title'] = 'Ubah password';
		$data['master_user'] = $this->db->get_where('master_user', ['username' => 
			$this->session->userdata('username')])->row_array();

		$this->form_validation->set_rules('password_lama', 'Password saat ini', 'required|trim',[
				'required' => 'password wajib diisi !'
			]);

		$this->form_validation->set_rules('password_baru', 'Ubah password', 'required|trim|min_length[6]|matches[konfirmasi_password]', [
				'matches' => 'Password berbeda!', 
				'min_length'=> 'Password min 6 karakter !',
				'required' => 'Password belum diisi !'
			]);

		$this->form_validation->set_rules('konfirmasi_password', 'Konfirmasi password', 'required|trim|matches[password_baru]', [
				'required' => 'Konfirmasi Password belum diisi !'
			]);

		if ($this->form_validation->run() == false) {
			
			$this->load->view('templates/header', $data);
	        $this->load->view('templates/menu');
	        $this->load->view('ganti_password', $data);
	        $this->load->view('templates/footer');
		}else{
			$password_lama = md5($this->input->post('password_lama'));
			// echo $password_lama; die();
			$password_baru = $this->input->post('password_baru');

			if ($password_lama != $data['master_user']['userpass']) {
				// echo $data['master_user']['userpass']; die();
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> "password saat ini" yang anda masukan salah!</div>');
				redirect('ganti_pass');
			}else{
				if ($password_lama == $password_baru) {
					$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> Gagal ! password baru tidak boleh sama dengan "Password saat ini"</div>');
					redirect('ganti_pass');
				}else{
					//password sudah ok
					$password_hash = md5($password_baru);

					$this->db->set('userpass', $password_hash);
					$this->db->where('username', $this->session->userdata('username'));
					$this->db->update('master_user');

					$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert"> Password anda telah diubah!</div>');
					redirect('ganti_pass');
				}
			}
		}

		
	}
}
