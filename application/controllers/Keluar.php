<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Keluar extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("keluar_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $keluar = $this->keluar_m;
        $validation = $this->form_validation;
        $validation->set_rules($keluar->rules());
        if ($validation->run()) {
            $keluar->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Surat Keluar berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("keluar");
        }
        $tgl_awal = $this->input->post('tgl_awal');
        $tgl_akhir = $this->input->post('tgl_akhir');
        $excel = $this->input->post('excel');

        if ($tgl_awal == '') $tgl_awal = '0000-00-00';
        if ($tgl_akhir == '') $tgl_akhir = '0000-00-00';

        $data["title"] = "Surat Keluar";
        $data["data_keluar"] = $this->keluar_m->getAll($tgl_awal, $tgl_akhir);

        $data["tgl_awal"] = $tgl_awal;
        $data["tgl_akhir"] = $tgl_akhir;
        
        if ($excel == '') {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/menu');
            $this->load->view('keluar/index', $data);
            $this->load->view('templates/footer');
        } else
            $this->load->view('keluar/excel', $data);
    }

    public function edit($id = null)
    {
        $this->form_validation->set_rules('no_surat', 'no_surat', 'required');
        $this->form_validation->set_rules('tgl_surat', 'tgl_surat', 'required');
        $this->form_validation->set_rules('sifat_surat', 'sifat_surat', 'required');
        $this->form_validation->set_rules('penerima', 'penerima', 'required');
        $this->form_validation->set_rules('isi', 'isi', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Surat Keluar gagal diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('keluar');
        } else {
            $config['upload_path']          = './uploads/';
            $config['allowed_types']        = 'jpg|jpeg|png|pdf';
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $data = $this->db->get_where('surat_keluar', ['id' => $id])->row_array();
                unlink("./uploads/" . $data['file']);

                $data = array('upload_data' => $this->upload->data());
                $file = $data['upload_data']['file_name'];

                $update = array(
                    "file" => $file,
                    "no_surat" => $this->input->post('no_surat'),
                    "tgl_surat" => $this->input->post('tgl_surat'),
                    "sifat_surat" => $this->input->post('sifat_surat'),
                    "penerima" => $this->input->post('penerima'),
                    "isi" => $this->input->post('isi'),
                    "tgl_update" => date('Y-m-d H:i:s'),
                    "user_update_by" => $this->session->userdata['username']
                );

                $this->db->where('id', $id);
                $this->db->update('surat_keluar', $update);
                $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                Data Surat Keluar berhasil diedit. 
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button></div>');
                redirect('keluar');
            } else {

                $update = array(
                    "no_surat" => $this->input->post('no_surat'),
                    "tgl_surat" => $this->input->post('tgl_surat'),
                    "sifat_surat" => $this->input->post('sifat_surat'),
                    "penerima" => $this->input->post('penerima'),
                    "isi" => $this->input->post('isi'),
                    "tgl_update" => date('Y-m-d H:i:s'),
                    "user_update_by" => $this->session->userdata['username']
                );

                $this->db->where('id', $id);
                $this->db->update('surat_keluar', $update);
                $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                Data Surat Keluar berhasil diedit. 
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button></div>');
                redirect('keluar');
            }
        }

    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Surat Keluar gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('keluar');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('surat_keluar');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Surat Keluar berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('keluar');
        }
    }

}
