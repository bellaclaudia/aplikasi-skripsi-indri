<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_user extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Master_user_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $user = $this->Master_user_m;

        $validation = $this->form_validation;
        $validation->set_rules($user->rules());
        if ($validation->run()) {
            $user->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data User berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("master_user");
        }
        $data["title"] = "User";

        $data['data_user'] = $this->Master_user_m->getAll();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('user/index', $data);
        $this->load->view('templates/footer');
    }
    
    public function edit($id)
    {
        $is_simpan = $this->input->post('is_simpan');

        if ($is_simpan == 1) {
            $passwdnya = $this->input->post('passwdnya');

            if (trim($passwdnya) != '') {
                $data = array(
                    "nama" => $this->input->post('nama'),
                    "bagian" => $this->input->post('bagian'),
                    "userpass" => md5($passwdnya),
                    "tgl_update" => date('Y-m-d H:i:s'),
                    "user_update_by" => $this->session->userdata['username']
                );
            } else {
                $data = array(
                    "nama" => $this->input->post('nama'),
                    "bagian" => $this->input->post('bagian'),
                    "tgl_update" => date('Y-m-d H:i:s'),
                    "user_update_by" => $this->session->userdata['username']
                );
            }

            $this->db->where('id', $id);
            $this->db->update('master_user', $data);

            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data User berhasil diupdate. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_user');
        } else {
        }


        $data["title"] = "Edit Data User";
        $data["data_user"] = $this->Master_user_m->get_user_by_id($id);
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('user/edit', $data);
        $this->load->view('templates/footer');
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data User gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_user');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('master_user');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data User berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_user');
        }
    }

    // 14-06-2022 ini udh ga dipake lg
    public function changepassword($id = null)
    {
        $this->form_validation->set_rules('userpass', 'userpass', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Password gagal diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_user');
        } else {
            $data = array(
                "id" => $this->input->post('id'),
                "userpass" => md5($this->input->post('userpass')),
                "tgl_update" => date('Y-m-d H:i:s'),
                "user_update_by" => $this->session->userdata['username']
            );
            $this->db->where('id', $this->input->post('id'));
            $this->db->update('master_user', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Password berhasil diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('master_user');
        }
    }
}
