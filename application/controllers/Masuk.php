<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Masuk extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("masuk_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $masuk = $this->masuk_m;
        $validation = $this->form_validation;
        $validation->set_rules($masuk->rules());
        if ($validation->run()) {
            $masuk->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Surat Masuk berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("masuk");
        }
        $tgl_awal = $this->input->post('tgl_awal');
        $tgl_akhir = $this->input->post('tgl_akhir');
        $excel = $this->input->post('excel');

        if ($tgl_awal == '') $tgl_awal = '0000-00-00';
        if ($tgl_akhir == '') $tgl_akhir = '0000-00-00';

        $data["title"] = "Surat Masuk";
        $data["data_masuk"] = $this->masuk_m->getAll($tgl_awal, $tgl_akhir);

        $data["tgl_awal"] = $tgl_awal;
        $data["tgl_akhir"] = $tgl_akhir;
        
        if ($excel == '') {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/menu');
            $this->load->view('masuk/index', $data);
            $this->load->view('templates/footer');
        } else
            $this->load->view('masuk/excel', $data);
    }

    public function edit($id = null)
    {
        $this->form_validation->set_rules('no_surat', 'no_surat', 'required');
        $this->form_validation->set_rules('tgl_surat', 'tgl_surat', 'required');
        $this->form_validation->set_rules('sifat_surat', 'sifat_surat', 'required');
        $this->form_validation->set_rules('pengirim', 'pengirim', 'required');
        $this->form_validation->set_rules('perihal', 'perihal', 'required');
        $this->form_validation->set_rules('diteruskan', 'diteruskan ke', 'required');
        $this->form_validation->set_rules('isi', 'isi', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Surat Masuk gagal diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('masuk');
        } else {
            $config['upload_path']          = './uploads/';
            $config['allowed_types']        = 'jpg|jpeg|png|pdf';
            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                $data = $this->db->get_where('surat_masuk', ['id' => $id])->row_array();
                unlink("./uploads/" . $data['file']);

                $data = array('upload_data' => $this->upload->data());
                $file = $data['upload_data']['file_name'];

                $update = array(
                    "file" => $file,
                    "no_surat" => $this->input->post('no_surat'),
                    "tgl_surat" => $this->input->post('tgl_surat'),
                    "sifat_surat" => $this->input->post('sifat_surat'),
                    "pengirim" => $this->input->post('pengirim'),
                    "perihal" => $this->input->post('perihal'),
                    "diteruskan" => $this->input->post('diteruskan'),
                    "isi" => $this->input->post('isi'),
                    "tgl_update" => date('Y-m-d H:i:s'),
                    "user_update_by" => $this->session->userdata['username']
                );

                $this->db->where('id', $id);
                $this->db->update('surat_masuk', $update);
                $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                Data Surat Masuk berhasil diedit. 
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button></div>');
                redirect('masuk');
            } else {

                $update = array(
                    "no_surat" => $this->input->post('no_surat'),
                    "tgl_surat" => $this->input->post('tgl_surat'),
                    "sifat_surat" => $this->input->post('sifat_surat'),
                    "pengirim" => $this->input->post('pengirim'),
                    "perihal" => $this->input->post('perihal'),
                    "diteruskan" => $this->input->post('diteruskan'),
                    "isi" => $this->input->post('isi'),
                    "tgl_update" => date('Y-m-d H:i:s'),
                    "user_update_by" => $this->session->userdata['username']
                );

                $this->db->where('id', $id);
                $this->db->update('surat_masuk', $update);
                $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
                Data Surat Masuk berhasil diedit. 
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button></div>');
                redirect('masuk');
            }
        }

    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Surat Masuk gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('masuk');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('surat_masuk');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Surat Masuk berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('masuk');
        }
    }

}
