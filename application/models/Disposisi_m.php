<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Disposisi_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'surat_disposisi';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'id_surat_masuk',  //samakan dengan atribute name pada tags input
                'label' => 'No. Surat Masuk',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'tgl_surat',  //samakan dengan atribute name pada tags input
                'label' => 'Tanggal Surat',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ]
        ];
    }

    public function getAll()
    {
        $this->db->select('a.*, b.no_surat as no_surat_masuk, b.isi, b.sifat_surat, b.perihal, b.diteruskan, b.tgl_surat as tgl_surat_masuk');
        $this->db->from('surat_disposisi a');
        $this->db->join('surat_masuk b','b.id = a.id_surat_masuk','left');
        $this->db->order_by("a.id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    public function getSuratMasuk()
    {
        return $this->db->get('surat_masuk')->result_array();
    }

    //menyimpan data mahasiswa
    public function save()
    {
        $id_surat_masuk = $this->input->post('id_surat_masuk');
        $sqlxx = " select id_surat_masuk FROM surat_disposisi WHERE id_surat_masuk = '$id_surat_masuk' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible fade show" role="alert">
            Data Surat Disposisi gagal diinput, karena no surat tersebut sudah ada di sistem. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('disposisi');
        }

        $data = array(
            "id_surat_masuk" => $this->input->post('id_surat_masuk'),
            "tgl_surat" => $this->input->post('tgl_surat'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->insert($this->table, $data);
    }

    public function getById($id)
    {
        return $this->db->get_where($this->table, ["id" => $id])->row();
    }

}