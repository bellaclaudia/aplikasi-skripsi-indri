<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Keluar_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'surat_keluar';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'no_surat',  //samakan dengan atribute name pada tags input
                'label' => 'No. Surat',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'tgl_surat',  //samakan dengan atribute name pada tags input
                'label' => 'Tanggal Surat',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'sifat_surat',  //samakan dengan atribute name pada tags input
                'label' => 'Sifat Surat',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'penerima',  //samakan dengan atribute name pada tags input
                'label' => 'Penerima',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'isi',  //samakan dengan atribute name pada tags input
                'label' => 'Isi',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ]
        ];
    }

    public function getAll($tgl_awal, $tgl_akhir)
    {
        $this->db->select('a.*');
        $this->db->from('surat_keluar a');
        if ($tgl_awal != '0000-00-00')
            $this->db->where("a.tgl_surat >=", $tgl_awal);
        if ($tgl_akhir != '0000-00-00')
            $this->db->where("a.tgl_surat <=", $tgl_akhir);
        $this->db->order_by("a.id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    //menyimpan data mahasiswa
    public function save()
    {
        $no_surat = $this->input->post('no_surat');
        $sqlxx = " select no_surat FROM surat_keluar WHERE no_surat = '$no_surat' ";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible fade show" role="alert">
            Data Surat Keluar gagal diinput, karena no surat tersebut sudah ada di sistem. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('keluar');
        }

        $config['allowed_types'] = 'jpg|jpeg|png|pdf';
        $config['overwrite'] = false;
        $config['max_size'] = 5120;
        //$config['encrypt_name'] = FALSE;
        $dir = './uploads/';
        if (!file_exists($dir)) {
            mkdir($dir, 777, true);
        }

        $config['upload_path'] = $dir;
        $timestamp = date('YmdHis');
        $config['file_name'] = 'SuratKeluar_' . $timestamp;
        $this->load->library('upload', $config);
        $error = 0;
        if (!empty($_FILES['file']['tmp_name'])) {

            $up = $this->upload->do_upload('file');
            if ($up) {
                $upl = $this->upload->data();
                $file = $upl['file_name'];

                unset($this->upload);
            } else {
                $error = 1;
            }
        } else {
            $file = '';
        }

        $data = array(
            "file" => $file,
            "no_surat" => $this->input->post('no_surat'),
            "tgl_surat" => $this->input->post('tgl_surat'),
            "sifat_surat" => $this->input->post('sifat_surat'),
            "penerima" => $this->input->post('penerima'),
            "isi" => $this->input->post('isi'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->insert($this->table, $data);
    }

    public function getById($id)
    {
        return $this->db->get_where($this->table, ["id" => $id])->row();
    }

}