<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item active">Data Surat Disposisi</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data Surat Disposisi
        </div>
        <div class="card-body">
          <button style="margin-left: 93%" class="btn btn-sm btn-success mb-1" type="button" data-toggle="modal" data-target="#tambahData">Input Disposisi</button><br><br>
          <?php if ($this->session->flashdata('message')) :
            echo $this->session->flashdata('message');
          endif; ?>
          <div style="overflow-x:auto;">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th></th>
                  <th>No. Surat</th>
                  <th>Tanggal Surat</th>
                  <th>Tanggal Penyelesaian</th>
                  <th>Diteruskan ke</th>
                  <th>Perihal</th>
                  <th>Isi Ringkas</th>
                  <th>Sifat Surat</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; ?>
                <?php foreach ($data_disposisi as $row) : ?>
                  <tr>
                    <td><?= $i++; ?></td>
                    <td><?= $row->no_surat_masuk ?></td>
                    <td><?= date('d-m-Y', strtotime($row->tgl_surat_masuk))?></td>
                    <td><?= date('d-m-Y', strtotime($row->tgl_surat))?></td>
                    <td><?= $row->diteruskan ?></td>
                    <td><?= $row->perihal ?></td>
                    <td><?= $row->isi ?></td>
                    <td><?= $row->sifat_surat ?></td>
                    <td>
                      <a data-toggle="modal" data-target="#modal-edit<?= $row->id; ?>" class="btn btn-success btn-circle" data-popup="tooltip" data-placement="top" title="Edit Data" data-popup="tooltip" data-placement="top">Edit</a>
                      <a href="<?php echo site_url('disposisi/hapus/' . $row->id); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data Surat Disposisi dengan No. Surat <?= $row->no_surat_masuk; ?> ?');" class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data">Hapus</a>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
          <div class="modal fade" id="tambahData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Tambah Data</h4>
                  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <?php
                  $attributes = array('id' => 'FrmAddKategori', 'method' => "post", "autocomplete" => "off", "class" => "form-horizontal", "enctype" => "multipart/form-data");
                  echo form_open('', $attributes);
                  ?>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">No. Surat Masuk</label>
                    <div class="col-md-9">
                      <select class="form-control select2-single" id="select2-1" name="id_surat_masuk">
                        <option value="0" selected disabled>-- No. Surat Masuk --</option>
                        <?php foreach ($surat_masuk as $k) : ?>
                          <option value="<?= $k['id']; ?>"><?= $k['no_surat']; ?></option>
                        <?php endforeach; ?>
                      </select>
                      <small class="text-danger">
                        <?php echo form_error('id_surat_masuk') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Tanggal Penyelesaian</label>
                    <div class="col-md-9">
                      <input class="form-control" type="date" name="tgl_surat" required>
                      <small class="text-danger">
                        <?php echo form_error('tgl_surat') ?>
                      </small>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <?php $no = 0;
          foreach ($data_disposisi as $row) : $no++; ?>
            <div class="modal fade" id="modal-edit<?= $row->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title">Edit Data</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form id="FrmAdd" class="form-horizontal" action="<?php echo site_url('disposisi/edit/'.$row->id); ?>" method="post" enctype="multipart/form-data" autocomplete="off">
	                     <div class="form-group row">
                        <label class="col-md-3 col-form-label">No. Surat Masuk</label>
                        <div class="col-md-9">
                          <select class="form-control select2-single" id="select2-3" name="id_surat_masuk">
                            <option value="0" disabled>-- No. Surat Masuk --</option>
                            <?php
                            $lv = '';
                            if (isset($row->id_surat_masuk)) {
                              $lv = $row->id_surat_masuk;
                            }
                            foreach ($surat_masuk as $r => $v) {
                            ?>
                              <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['no_surat'] ?></option>
                            <?php
                            }
                            ?>
                          </select>
                          <small class="text-danger">
                            <?php echo form_error('id_surat_masuk') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Tanggal Penyelesaian</label>
                        <div class="col-md-9">
                          <input class="form-control" type="date" name="tgl_surat" required value="<?= $row->tgl_surat; ?>">
                          <small class="text-danger">
                            <?php echo form_error('tgl_surat') ?>
                          </small>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                        <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
</main>
</div>