<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item"><a href="<?= base_url('dashboard'); ?>">Dashboard</a></li>
    <li class="breadcrumb-item active">Ganti Password</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-plus"></i> Ganti Password
        </div>
        <div class="card-body">
          <?php if ($this->session->flashdata('message')) :
              echo $this->session->flashdata('message');
            endif; ?><br>
          <form action="<?php echo base_url('ganti_pass/changepassword'); ?>" method="post">
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Password Lama</label>
            <div class="col-md-9">
              <input class="form-control" type="password" name="password_lama" id="password_lama" placeholder="Isi Password Lama" value="<?= set_value('password_lama'); ?>">
              <small class="text-danger">
                <?php echo form_error('password_lama') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Password Baru</label>
            <div class="col-md-9">
              <input class="form-control" type="password" name="password_baru" id="password_baru" placeholder="Isi Password Baru" value="<?= set_value('password_baru'); ?>">
              <small class="text-danger">
                <?php echo form_error('password_baru') ?>
              </small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-md-3 col-form-label">Konfirmasi Password</label>
            <div class="col-md-9">
              <input class="form-control" type="password" name="konfirmasi_password" id="konfirmasi_password" placeholder="Isi Konfirmasi Password" value="<?= set_value('konfirmasi_password'); ?>">
              <small class="text-danger">
                <?php echo form_error('konfirmasi_password') ?>
              </small>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit"><i class="fa fa-plus"></i> Simpan</button>&nbsp;
            <a href="<?= base_url('dashboard'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Batal</a>
          </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>
</div>