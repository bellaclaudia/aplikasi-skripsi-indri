<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item active">Data Surat Keluar</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data Surat Keluar
        </div>
        <div class="card-body">
          <?php if ($this->session->flashdata('message')) :
            echo $this->session->flashdata('message');
          endif; ?>

          <b>Filter Pencarian</b>
            <?php
            $attributes = array('id' => 'FrmPencarian', 'method' => "post", "autocomplete" => "off");
            echo form_open('', $attributes);
            ?>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tanggal Awal</label>
              <div class="col-md-2">
                <input class="form-control" type="date" name="tgl_awal" value="<?=$tgl_awal?>">
                <small class="text-danger">
                  <?php echo form_error('tgl_awal') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Tanggal Akhir</label>
              <div class="col-md-2">
                <input class="form-control" type="date" name="tgl_akhir" value="<?=$tgl_akhir?>">
                <small class="text-danger">
                  <?php echo form_error('tgl_akhir') ?>
                </small>
              </div>
            </div>
            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit">Cari</button>&nbsp;
            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit" name="excel" value="1">Export Excel</button>&nbsp;
            </form>
            <hr>
            <button style="margin-left: 93%" class="btn btn-sm btn-success mb-1" type="button" data-toggle="modal" data-target="#tambahData">Input Surat</button><br><br>
          <div style="overflow-x:auto;">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th></th>
                  <th>Nama File</th>
                  <th>No. Surat</th>
                  <th>Tanggal Surat</th>
                  <th>Sifat Surat</th>
                  <th>Penerima</th>
                  <th>Isi</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; ?>
                <?php foreach ($data_keluar as $row) : ?>
                  <tr>
                    <td><?= $i++; ?></td>
                    <td>
                      <?php if ($row->file != '') { ?>
                      <center><a href="<?= base_url('uploads/' . $row->file) ?>" target="_blank"><img src="<?= base_url('uploads/' . $row->file) ?>" alt="<?= $row->file ?>" width="100"></a><br>
                      <a href="<?= base_url('uploads/' . $row->file) ?>" download>Download</a></center>
                      <?php } ?>
                    </td>
                    <td><?= $row->no_surat ?></td>
                    <td><?= date('d-m-Y', strtotime($row->tgl_surat))?></td>
                    <td><?= $row->sifat_surat ?></td>
                    <td><?= $row->penerima ?></td>
                    <td><?= $row->isi ?></td>
                    <td>
                      <a data-toggle="modal" data-target="#modal-edit<?= $row->id; ?>" class="btn btn-success btn-circle" data-popup="tooltip" data-placement="top" title="Edit Data" data-popup="tooltip" data-placement="top">Edit</a>
                      <a href="<?php echo site_url('keluar/hapus/' . $row->id); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data Surat Keluar dengan No. Surat <?= $row->no_surat; ?> ?');" class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data">Hapus</a>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
          <div class="modal fade" id="tambahData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Tambah Data</h4>
                  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <?php
                  $attributes = array('id' => 'FrmAddBarang', 'method' => "post", "autocomplete" => "off", "class" => "form-horizontal", "enctype" => "multipart/form-data");
                  echo form_open('', $attributes);
                  ?>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">File</label>
                    <div class="col-md-9">
                      <input class="form-control" type="file" name="file" accept=".jpg,.png,.jpeg,.pdf" required>
                      <span style="color: red">*File type = .jpg,.png,.jpeg,.pdf</span>
                      <small class="text-danger">
                        <?php echo form_error('file') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">No. Surat</label>
                    <div class="col-md-9">
                      <input class="form-control" type="text" name="no_surat" placeholder="No. Surat" required>
                      <small class="text-danger">
                        <?php echo form_error('no_surat') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Tanggal Surat</label>
                    <div class="col-md-9">
                      <input class="form-control" type="date" name="tgl_surat" required>
                      <small class="text-danger">
                        <?php echo form_error('tgl_surat') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Sifat Surat</label>
                    <div class="col-md-9">
                      <input type="radio" name="sifat_surat" value="Rahasia" required> Rahasia&nbsp;&nbsp;&nbsp;
                      <input type="radio" name="sifat_surat" value="Penting" required> Penting&nbsp;&nbsp;&nbsp;
                      <input type="radio" name="sifat_surat" value="Biasa" required> Biasa
                      <small class="text-danger">
                        <?php echo form_error('sifat_surat') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Penerima</label>
                    <div class="col-md-9">
                      <input class="form-control" type="text" name="penerima" placeholder="Isi Penerima" required>
                      <small class="text-danger">
                        <?php echo form_error('penerima') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Isi</label>
                    <div class="col-md-9">
                      <input class="form-control" type="text" name="isi" placeholder="Isi" required>
                      <small class="text-danger">
                        <?php echo form_error('isi') ?>
                      </small>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <?php $no = 0;
          foreach ($data_keluar as $row) : $no++; ?>
            <div class="modal fade" id="modal-edit<?= $row->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title">Edit Data</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form id="FrmAdd" class="form-horizontal" action="<?php echo site_url('keluar/edit/'.$row->id); ?>" method="post" enctype="multipart/form-data" autocomplete="off">
      	              <div class="form-group row">
                        <label class="col-md-3 col-form-label">File</label>
                        <div class="col-md-9">
                          <input class="form-control" type="hidden" name="id" value="<?= $row->id; ?>">
                          <input class="form-control" type="file" name="file" accept=".jpg,.png,.jpeg,.pdf" required>
                          <span style="color: red">*File type = .jpg,.png,.jpeg,.pdf</span>
                          <small class="text-danger">
                            <?php echo form_error('file') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">No. Surat</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="no_surat" placeholder="No. Surat" value="<?= $row->no_surat; ?>" required>
                          <small class="text-danger">
                            <?php echo form_error('no_surat') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Tanggal Surat</label>
                        <div class="col-md-9">
                          <input class="form-control" type="date" name="tgl_surat" required value="<?= $row->tgl_surat; ?>">
                          <small class="text-danger">
                            <?php echo form_error('tgl_surat') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Sifat Surat</label>
                        <div class="col-md-9">
                          <input type="radio" name="sifat_surat" value="Rahasia" <?php if($row->sifat_surat=='Rahasia') echo 'checked'?>> Rahasia&nbsp;&nbsp;&nbsp;
                          <input type="radio" name="sifat_surat" value="Penting" <?php if($row->sifat_surat=='Penting') echo 'checked'?>> Penting&nbsp;&nbsp;&nbsp;
                          <input type="radio" name="sifat_surat" value="Biasa" <?php if($row->sifat_surat=='Biasa') echo 'checked'?>> Biasa&nbsp;&nbsp;&nbsp;
                          <small class="text-danger">
                            <?php echo form_error('sifat_surat') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Penerima</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="penerima" placeholder="Isi Penerima" required value="<?= $row->penerima; ?>">
                          <small class="text-danger">
                            <?php echo form_error('penerima') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Isi</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="isi" placeholder="Isi" required value="<?= $row->isi; ?>">
                          <small class="text-danger">
                            <?php echo form_error('isi') ?>
                          </small>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                        <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
</main>
</div>