<!DOCTYPE html>
<html lang="en">

<head>
  <base href="./">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
  <meta name="author" content="Łukasz Holeczek">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
  <title>.:: Dinas PUBMTR ::.</title>
  <!-- Icons-->
  <link href="<?= base_url(); ?>assets/node_modules/@coreui/icons/css/coreui-icons.min.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/node_modules/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/node_modules/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/node_modules/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
  <!-- Main styles for this application-->
  <link href="<?= base_url(); ?>assets/css/style.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/vendors/pace-progress/css/pace.min.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/css/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
  <link href="<?= base_url(); ?>assets/vendors/select2/css/select2.min.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/node_modules/ladda/dist/ladda-themeless.min.css" rel="stylesheet">
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
  <header style="padding: 10px">
    <h3><img src="<?= base_url(); ?>assets/img/logo.png" style="width: 100px"><b>Dinas Pekerjaan Umum Bina Marga dan Tata Ruang Provinsi Sumatera Selatan</b></h3>
  </header><br><br><br><br><br><br><br><br><br>
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <div class="card-group">
          <div class="card p-4">
            <div class="card-body">
              <center>
                <h2>Selamat Datang di Arsip Surat Masuk dan Surat Keluar Bidang Jembatan</h2>
              </center><br>
              <?= $this->session->flashdata('pesan') ?>
              <form method="post" action="<?= base_url('login/proses_login') ?>">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="icon-user"></i>
                    </span>
                  </div>
                  <input class="form-control" type="text" name="username" placeholder="Username">
                </div>
                <div class="input-group mb-4">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="icon-lock"></i>
                    </span>
                  </div>
                  <input class="form-control" type="password" name="password" placeholder="Password">
                </div>
                <div class="row">
                  <div class="col-12">
                    <button class="btn btn-primary btn-ladda" data-style="expand-right" name="login"><i class="fa fa-user"></i> LOGIN</button>
                  </div>
                </div>
              </form>
            </div>
            <center>
              <p><?= date('Y') ?> &copy;. <a href="#" target="_blank">Dinas Pekerjaan Umum Bina Marga dan Tata Ruang Provinsi Sumatera Selatan.</a></p>
            </center>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- CoreUI and necessary plugins-->
  <script src="<?= base_url(); ?>assets/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="<?= base_url(); ?>assets/node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?= base_url(); ?>assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?= base_url(); ?>assets/node_modules/pace-progress/pace.min.js"></script>
    <script src="<?= base_url(); ?>assets/node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
    <script src="<?= base_url(); ?>assets/node_modules/@coreui/coreui-pro/dist/js/coreui.min.js"></script>
    <!-- Plugins and scripts required by this view-->
    <script src="<?= base_url(); ?>assets/node_modules/chart.js/dist/Chart.min.js"></script>
    <script src="<?= base_url(); ?>assets/node_modules/@coreui/coreui-plugin-chartjs-custom-tooltips/dist/js/custom-tooltips.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/main.js"></script>
    <script src="<?= base_url(); ?>assets/node_modules/jquery.maskedinput/src/jquery.maskedinput.js"></script>
    <script src="<?= base_url(); ?>assets/node_modules/moment/min/moment.min.js"></script>
    <script src="<?= base_url(); ?>assets/node_modules/select2/dist/js/select2.min.js"></script>
    <script src="<?= base_url(); ?>assets/node_modules/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?= base_url(); ?>assets/js/advanced-forms.js"></script>
    <script src="<?= base_url(); ?>assets/node_modules/ladda/dist/spin.min.js"></script>
    <script src="<?= base_url(); ?>assets/node_modules/ladda/dist/ladda.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/loading-buttons.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.dataTables.js"></script>
    <script src="<?= base_url(); ?>assets/js/dataTables.bootstrap4.js"></script>
    <script src="<?= base_url(); ?>assets/js/datatables.js"></script>
</body>

</html>