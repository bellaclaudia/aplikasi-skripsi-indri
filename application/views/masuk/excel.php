<?php
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=surat_keluar.xls");
header("Cache-control: public");
?>
<style>
    .allcen{
        text-align: center !important;
        vertical-align: middle !important;
        position: relative !important;
    }
    .allcen2{
        text-align: center !important;
        vertical-align: middle !important;
        position: relative !important;
    }
    .str{ 
        mso-number-format:\@; 
    }
</style>
<head>
    <meta charset="utf-8" />
    <title>Dinas PUBMTR | Surat Masuk</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <style>
        body {
            font-family: verdana,arial,sans-serif;
            font-size: 14px;
            line-height: 20px;
            font-weight: 400;
            -webkit-font-smoothing: antialiased;
            font-smoothing: antialiased;
        }
        table.gridtable {
            font-family: verdana,arial,sans-serif;
            font-size:11px;
            width: 100%;
            color:#333333;
            border-width: 1px;
            border-color: #e9e9e9;
            border-collapse: collapse;
        }
        table.gridtable th {
            border-width: 1px;
            padding: 8px;
            font-size:12px;
            border-style: solid;
            font-weight: 900;
            color: #ffffff;
            border-color: #e9e9e9;
            background: #ea6153;
        }
        table.gridtable td {
            border-width: 1px;
            padding: 8px;
            border-style: solid;
            border-color: #e9e9e9;
            background-color: #ffffff;
        }

    </style>
</head>
<body>
<h4 style="text-align: center">Surat Masuk pada Dinas Pekerjaan Umum Bina Marga dan Tata Ruang Provinsi Sumatera Selatan</h4>
<table border="1" width="100%" class="gridtable">
  <thead style="background-color: blue">
    <tr>
      	<th class="allcen center bold">No.</th>
        <th class="allcen center bold">No. Surat</th>
        <th class="allcen center bold">Tanggal Surat</th>
        <th class="allcen center bold">Sifat Surat</th>
        <th class="allcen center bold">Pengirim</th>
        <th class="allcen center bold">Perihal</th>
        <th class="allcen center bold">Diteruskan ke</th>
        <th class="allcen center bold">Isi</th>
    </tr>
  </thead>
  <tbody>
    <?php $i = 1; ?>
    <?php foreach ($data_masuk as $row) : ?>
      <tr>
        <td><?= $i++; ?></td>
        <td><?= $row->no_surat ?></td>
        <td><?= date('d-m-Y', strtotime($row->tgl_surat))?></td>
        <td><?= $row->sifat_surat ?></td>
        <td><?= $row->pengirim ?></td>
        <td><?= $row->perihal ?></td>
        <td><?= $row->diteruskan ?></td>
        <td><?= $row->isi ?></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>