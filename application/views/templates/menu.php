<div class="app-body">
  <div class="sidebar">
    <nav class="sidebar-nav">
      <ul class="nav">
        <li class="nav-title">Menu</li>
        <?php if (isset($this->session->userdata['logged_in'])) { // jika udh lgin 
        ?>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('dashboard'); ?>">
              <i class="nav-icon icon-speedometer"></i> Dashboard
            </a>
          </li>
          <li class="nav-item nav-dropdown">
            <a class="nav-link nav-dropdown-toggle" href="#">
              <i class="nav-icon fa fa-share-alt"></i> Pengaturan</a>
            <ul class="nav-dropdown-items">
              <li class="nav-item">
                <a class="nav-link" href="<?= base_url('master_user'); ?>">
                  <i class="nav-icon fa fa-user"></i> Data User
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?= base_url('ganti_pass') ?>">
                  <i class="nav-icon fa fa-users"></i> Ganti Password
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('keluar'); ?>">
              <i class="nav-icon icon-list"></i> Surat Keluar
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('masuk'); ?>">
              <i class="nav-icon icon-list"></i> Surat Masuk
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('disposisi'); ?>">
              <i class="nav-icon icon-list"></i> Disposisi
            </a>
          </li>
        <?php }
        ?>
      </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
  </div>