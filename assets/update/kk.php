<?php
session_start();
include '../koneksi.php';

$id_kk = addslashes($_POST['id_kk']);
$filename1 = addslashes($_FILES['ft_akta_nikah']['size']);
$filename2 = addslashes($_FILES['ft_ket_lurah']['size']);
$alasan = addslashes($_POST['alasan']);

$temp = explode(".", $_FILES["ft_akta_nikah"]["name"]);
$size = explode(".", $_FILES["ft_akta_nikah"]["size"]);
$type = explode(".", $_FILES["ft_akta_nikah"]["type"]);
$ft_akta_nikah = 'Foto_akta_nikah_'.round(microtime(true)) . '.' . end($temp);
move_uploaded_file($_FILES["ft_akta_nikah"]["tmp_name"], "../img/kk/" . $ft_akta_nikah);

$temp = explode(".", $_FILES["ft_ket_lurah"]["name"]);
$size = explode(".", $_FILES["ft_ket_lurah"]["size"]);
$type = explode(".", $_FILES["ft_ket_lurah"]["type"]);
$ft_ket_lurah = 'Foto_ket_lurah_'.round(microtime(true)) . '.' . end($temp);
move_uploaded_file($_FILES["ft_ket_lurah"]["tmp_name"], "../img/kk/" . $ft_ket_lurah);

$old_filename1 = $_POST['old_ft_akta_nikah'];
$old_filename2 = $_POST['old_ft_ket_lurah'];

if(($filename1 <= 0) && ($filename2 <= 0)) //jika foto kosong atau tidak di ganti
{
    $query = mysqli_query($koneksi, "UPDATE kartu_keluarga SET ft_akta_nikah= '$old_filename1', ft_ket_lurah='$old_filename2', alasan='$alasan' WHERE id_kk='$id_kk'");
    echo "
            <script>
                alert('Data Berhasil Diupdate');
                window.location=history.go(-2);
            </script>
            ";
}

elseif (($filename1 > 0) && ($filename2 > 0)) // jika foto di ganti
{
    $query = mysqli_query($koneksi, "UPDATE kartu_keluarga SET ft_akta_nikah='$ft_akta_nikah', ft_ket_lurah='$ft_ket_lurah', alasan='$alasan' WHERE id_kk='$id_kk'");
    echo "
            <script>
                alert('Data Berhasil Diupdate');
                window.location=history.go(-2);
            </script>
            ";
}elseif ($filename1 > 0) // jika gambar di ganti
{
    $query = mysqli_query($koneksi, "UPDATE kartu_keluarga SET ft_akta_nikah='$ft_akta_nikah', ft_ket_lurah='$old_filename2', alasan='$alasan' WHERE id_kk='$id_kk' ");
    echo "
            <script>
                alert('Data Berhasil Diupdate');
                window.location=history.go(-2);
            </script>
            ";
}elseif ($filename2 > 0) // jika gambar di ganti
{
    $query = mysqli_query($koneksi, "UPDATE kartu_keluarga SET ft_akta_nikah='$old_filename1', ft_ket_lurah='$ft_ket_lurah', alasan='$alasan' WHERE id_kk='$id_kk' ");
    echo "
            <script>
                alert('Data Berhasil Diupdate');
                window.location=history.go(-2);
            </script>
            ";
}
else 
            {
                echo "
                <script>
                    alert('Data Gagal Diupdate !');
                    document.location.href ='../edit-kartu-keluarga';
                </script>";
            }
?>