<?php
session_start();
include '../koneksi.php';

$id_kurir = addslashes($_POST['id_kurir']);
$nama_kurir = addslashes($_POST['nama_kurir']);
$nik = addslashes($_POST['nik']);
$filename1 = addslashes($_FILES['foto_ktp']['size']);
$alamat = addslashes($_POST['alamat']);
$no_hp = addslashes($_POST['no_hp']);
$filename2 = addslashes($_FILES['foto_sim']['size']);
$filename3 = addslashes($_FILES['foto_diri']['size']);

$temp = explode(".", $_FILES["foto_ktp"]["name"]);
$size = explode(".", $_FILES["foto_ktp"]["size"]);
$type = explode(".", $_FILES["foto_ktp"]["type"]);
$foto_ktp = 'Foto_ktp_'.round(microtime(true)) . '.' . end($temp);
move_uploaded_file($_FILES["foto_ktp"]["tmp_name"], "../img/ktp/" . $foto_ktp);

$temp = explode(".", $_FILES["foto_sim"]["name"]);
$size = explode(".", $_FILES["foto_sim"]["size"]);
$type = explode(".", $_FILES["foto_sim"]["type"]);
$foto_sim = 'Foto_sim_'.round(microtime(true)) . '.' . end($temp);
move_uploaded_file($_FILES["foto_sim"]["tmp_name"], "../img/sim/" . $foto_sim);

$temp = explode(".", $_FILES["foto_diri"]["name"]);
$size = explode(".", $_FILES["foto_diri"]["size"]);
$type = explode(".", $_FILES["foto_diri"]["type"]);
$foto_diri = 'Foto_diri_'.round(microtime(true)) . '.' . end($temp);
move_uploaded_file($_FILES["foto_diri"]["tmp_name"], "../img/foto_diri/" . $foto_diri);

$old_filename1 = $_POST['old_foto_ktp'];
$old_filename2 = $_POST['old_foto_sim'];
$old_filename3 = $_POST['old_foto_diri'];

if(($filename1 <= 0) && ($filename2 <= 0) && ($filename3 <= 0)) //jika foto kosong atau tidak di ganti
{
    $query = mysqli_query($koneksi, "UPDATE kurir SET nama_kurir='$nama_kurir', nik='$nik', foto_ktp='$old_filename1', alamat='$alamat', no_hp='$no_hp', foto_sim='$old_filename2', foto_diri='$old_filename3' WHERE id_kurir='$id_kurir'");
    echo "
            <script>
                alert('Data Berhasil Diupdate');
                window.location=history.go(-2);
            </script>
            ";
}

elseif (($filename1 > 0) && ($filename2 > 0) && ($filename3 > 0)) // jika foto di ganti
{
    $query = mysqli_query($koneksi, "UPDATE kurir SET nama_kurir='$nama_kurir', nik='$nik', foto_ktp='$foto_ktp', alamat='$alamat', no_hp='$no_hp', foto_sim='$foto_sim', foto_diri='$foto_diri' WHERE id_kurir='$id_kurir'");
    echo "
            <script>
                alert('Data Berhasil Diupdate');
                window.location=history.go(-2);
            </script>
            ";
}elseif ($filename1 > 0) // jika gambar di ganti
{
    $query = mysqli_query($koneksi, "UPDATE kurir SET nama_kurir='$nama_kurir', nik='$nik', foto_ktp='$foto_ktp', alamat='$alamat', no_hp='$no_hp', foto_sim='$old_filename2', foto_diri='$old_filename3' WHERE id_kurir='$id_kurir' ");
    echo "
            <script>
                alert('Data Berhasil Diupdate');
                window.location=history.go(-2);
            </script>
            ";
}elseif ($filename2 > 0) // jika gambar di ganti
{
    $query = mysqli_query($koneksi, "UPDATE kurir SET nama_kurir='$nama_kurir', nik='$nik', foto_ktp='$old_filename1', alamat='$alamat', no_hp='$no_hp', foto_sim='$foto_sim', foto_diri='$old_filename3' WHERE id_kurir='$id_kurir' ");
    echo "
            <script>
                alert('Data Berhasil Diupdate');
                window.location=history.go(-2);
            </script>
            ";
}elseif ($filename3 > 0) // jika gambar di ganti
{
    $query = mysqli_query($koneksi, "UPDATE kurir SET nama_kurir='$nama_kurir', nik='$nik', foto_ktp='$old_filename1', alamat='$alamat', no_hp='$no_hp', foto_sim='$old_filename2', foto_diri='$foto_diri' WHERE id_kurir='$id_kurir' ");
    echo "
            <script>
                alert('Data Berhasil Diupdate');
                window.location=history.go(-2);
            </script>
            ";
}
else 
            {
                echo "
                <script>
                    alert('Data Gagal Diupdate !');
                    window.location=history.go(-1);
                </script>";
            }
?>