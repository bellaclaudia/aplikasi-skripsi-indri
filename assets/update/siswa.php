<?php
session_start();
include '../koneksi.php';

$id_siswa = addslashes($_POST['id_siswa']);
$id_master = addslashes($_POST['id_master']);
$tmp_lahir = addslashes($_POST['tmp_lahir']);
$tgl_lahir = addslashes($_POST['tgl_lahir']);
$nis = addslashes($_POST['nis']);
$asal_sekolah = addslashes($_POST['asal_sekolah']);
$alamat = addslashes($_POST['alamat']);
$no_hp = addslashes($_POST['no_hp']);
$filename = addslashes($_FILES['surat_pengantar']['size']);

$temp = explode(".", $_FILES["surat_pengantar"]["name"]);
$size = explode(".", $_FILES["surat_pengantar"]["size"]);
$type = explode(".", $_FILES["surat_pengantar"]["type"]);
$surat_pengantar = 'Surat_Pengantar_'.round(microtime(true)) . '.' . end($temp);
move_uploaded_file($_FILES["surat_pengantar"]["tmp_name"], "../img/" . $surat_pengantar);

$old_filename = $_POST['old_surat_pengantar'];

if($filename <= 0) //jika foto kosong atau tidak di ganti
{
    $query = mysqli_query($koneksi, "UPDATE siswa SET id_master='$id_master', tmp_lahir='$tmp_lahir', tgl_lahir='$tgl_lahir', nis='$nis', asal_sekolah='$asal_sekolah', alamat='$alamat', no_hp='$no_hp', surat_pengantar='$old_filename' WHERE id_siswa='$id_siswa'");
    echo "
            <script>
                alert('Data Berhasil Diupdate');
                window.location=history.go(-2);
            </script>
            ";
}

elseif ($filename > 0) // jika foto di ganti
{
    $query = mysqli_query($koneksi, "UPDATE siswa SET tmp_lahir='$tmp_lahir', tgl_lahir='$tgl_lahir', nis='$nis', asal_sekolah='$asal_sekolah', alamat='$alamat', tgl_mulai='$tgl_mulai', tgl_selesai='$tgl_selesai', no_hp='$no_hp', surat_pengantar='$surat_pengantar' WHERE id_siswa='$id_siswa'");
    echo "
            <script>
                alert('Data Berhasil Diupdate');
                window.location=history.go(-2);
            </script>
            ";
}
else 
            {
                echo "
                <script>
                    alert('Data Gagal Diupdate !');
                    document.location.href ='../edit-siswa';
                </script>";
            }
?>