-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 02 Agu 2022 pada 13.28
-- Versi server: 10.4.24-MariaDB
-- Versi PHP: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `indri`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_user`
--

CREATE TABLE `master_user` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `bagian` varchar(50) NOT NULL,
  `username` varchar(30) NOT NULL,
  `userpass` text NOT NULL,
  `status_login` int(11) NOT NULL DEFAULT 0 COMMENT '0: blm login/sudah logout\r\n1: login',
  `usertoken` text DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `last_logout` datetime DEFAULT NULL,
  `tgl_input` datetime NOT NULL,
  `tgl_update` datetime NOT NULL,
  `user_update_by` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `master_user`
--

INSERT INTO `master_user` (`id`, `nama`, `bagian`, `username`, `userpass`, `status_login`, `usertoken`, `last_login`, `last_logout`, `tgl_input`, `tgl_update`, `user_update_by`) VALUES
(1, 'Indri', 'SDM', 'admin', '0192023a7bbd73250516f069df18b500', 1, NULL, '2022-08-02 04:53:50', NULL, '2022-07-17 10:07:00', '2022-07-19 08:35:33', 'admin'),
(2, 'Bella Claudia', 'SDM', 'admin2', '2f3dfb02cd1cea0f29398e62669637ab', 0, NULL, NULL, NULL, '2022-07-17 05:45:09', '2022-07-19 08:35:21', 'admin'),
(4, 'Dilla', 'SDM', 'admindila', 'e10adc3949ba59abbe56e057f20f883e', 1, NULL, '2022-07-19 15:44:42', NULL, '2022-07-19 08:36:27', '0000-00-00 00:00:00', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `surat_disposisi`
--

CREATE TABLE `surat_disposisi` (
  `id` int(11) NOT NULL,
  `id_surat_masuk` int(11) NOT NULL,
  `no_surat` varchar(20) NOT NULL,
  `tgl_surat` date NOT NULL,
  `diteruskan` varchar(100) NOT NULL,
  `perihal` varchar(100) NOT NULL,
  `sifat_surat` varchar(20) NOT NULL,
  `tgl_input` datetime NOT NULL,
  `tgl_update` datetime NOT NULL,
  `user_update_by` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `surat_disposisi`
--

INSERT INTO `surat_disposisi` (`id`, `id_surat_masuk`, `no_surat`, `tgl_surat`, `diteruskan`, `perihal`, `sifat_surat`, `tgl_input`, `tgl_update`, `user_update_by`) VALUES
(1, 1, 'test747489', '2022-07-22', 'test', 'test', 'Rahasia', '2022-07-19 08:26:57', '2022-07-19 08:30:13', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `surat_keluar`
--

CREATE TABLE `surat_keluar` (
  `id` int(11) NOT NULL,
  `file` text NOT NULL,
  `no_surat` varchar(20) NOT NULL,
  `tgl_surat` date NOT NULL,
  `sifat_surat` varchar(20) NOT NULL,
  `penerima` varchar(50) NOT NULL,
  `isi` varchar(100) NOT NULL,
  `tgl_input` datetime NOT NULL,
  `tgl_update` datetime NOT NULL,
  `user_update_by` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `surat_keluar`
--

INSERT INTO `surat_keluar` (`id`, `file`, `no_surat`, `tgl_surat`, `sifat_surat`, `penerima`, `isi`, `tgl_input`, `tgl_update`, `user_update_by`) VALUES
(1, 'heru1.png', 'test', '2022-07-19', 'Penting', 'test', 'test', '2022-07-19 07:23:41', '2022-07-19 07:29:15', 'admin'),
(2, 'SuratKeluar_20220719073251.jpg', 'test1', '2022-07-23', 'Biasa', 'test', 'test', '2022-07-19 07:32:51', '0000-00-00 00:00:00', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `surat_masuk`
--

CREATE TABLE `surat_masuk` (
  `id` int(11) NOT NULL,
  `file` text NOT NULL,
  `no_surat` varchar(20) NOT NULL,
  `tgl_surat` date NOT NULL,
  `sifat_surat` varchar(20) NOT NULL,
  `pengirim` varchar(50) NOT NULL,
  `perihal` varchar(100) NOT NULL,
  `diteruskan` varchar(50) NOT NULL,
  `isi` varchar(100) NOT NULL,
  `tgl_input` datetime NOT NULL,
  `tgl_update` datetime NOT NULL,
  `user_update_by` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `surat_masuk`
--

INSERT INTO `surat_masuk` (`id`, `file`, `no_surat`, `tgl_surat`, `sifat_surat`, `pengirim`, `perihal`, `diteruskan`, `isi`, `tgl_input`, `tgl_update`, `user_update_by`) VALUES
(1, 'irwan.png', 'test123', '2022-07-19', 'Biasa', 'test', 'test', 'test', 'test', '2022-07-19 08:03:10', '2022-07-19 08:05:53', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `master_user`
--
ALTER TABLE `master_user`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `surat_disposisi`
--
ALTER TABLE `surat_disposisi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `surat_keluar`
--
ALTER TABLE `surat_keluar`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `surat_masuk`
--
ALTER TABLE `surat_masuk`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `master_user`
--
ALTER TABLE `master_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `surat_disposisi`
--
ALTER TABLE `surat_disposisi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `surat_keluar`
--
ALTER TABLE `surat_keluar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `surat_masuk`
--
ALTER TABLE `surat_masuk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
